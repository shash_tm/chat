var io = require('socket.io-client');
var request = require('request');
var  ini = require('ini')
var fs=require('fs');
var redis = require("redis"),

config = (function() {
	return ini.parse(fs.readFileSync('/var/www/html/config/config.ini', 'utf-8'))
})();


redis_client = redis.createClient(config.redis.port,config.redis.ip);

redis_client.on("error", function (err) {
    console.log("Error: Redis " + err);
});


var ip=config.urls.chat_server_url;
var serverPort='80';
if(ip.startsWith('https')){
	serverPort='443';
}
console.log('Server port: '+serverPort)
var socket = io.connect(ip, {
        query :'header={"app_version_code":"75","cookie":[{"value":"'+config.admin.miss_tm_secret+'","key":"MISS_TM_SECRET"}],"device_id":"1b1fc14bae14a1e1", "source": "android_app","app_version_code": "2.0.2.5","os_version_code": "4.4.4"}',
            port: serverPort}
);

socket.on('connect', function () {
    console.log("socket connected with ");
});


socket.on('error',function (obj) {
    // does not fire
    console.log("connect_error" +obj.toString());
});



socket.on('disconnect',function(){
         console.log("MISS TM Disconnected ");
});

socket.on('chat_received', function(data){
	var unique_id = data.unique_id;
	redis_client.setnx("miss_tm_"+unique_id,15,function(err, reply){
		if(reply == 1){
			redis_client.expire("miss_tm_"+unique_id, 15);
		/*	socket.emit("chat_typing", {receiver_id:data.sender_id},function callback(data){
				//do nothing
			});
		*/	
			request.post({url:config.urls.baseurl+'/msg/misstm/chatbot/conversation_start.php', form: {say:data.msg, convo_id:data.sender_id}}, 
					function(err,httpResponse,body){
						//console.log(body);
						body = JSON.parse(body);
						if(body != undefined && body !=null && body.botsay){
							socket.emit('chat_sent', {"timestamp":new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),"unique_id":Math.random(), "msg":body.botsay,"message_type":body.type,"receiver_id":data.sender_id}, 
									function callback(data){
								}
							);
						}
					}
			);
		}
	});
})

