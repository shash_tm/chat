﻿/**
 * Module dependencies.
 */

var Q = require("q");
//global variables
require('console-stamp')(console, '[HH:MM:ss.l]');

var numCPUs = require('os').cpus().length;

//custom modules
var service = require('./lib/service');
var util = require('./lib/util');

var config = util.parsedConfig;

//var http = require('http');
var mysql      = require('mysql');
//var connection;
var pool;
numCPUs=require('os').cpus().length;
var sticky = require('sticky-session');
var sitcky_cpu_number=numCPUs==1?numCPUs:numCPUs-1;
var sticky_options = {
		  proxy: true, //activate layer 4 patching
		  header: 'x-forwarded-for', //provide here your header containing the users ip
		  workers: sitcky_cpu_number, //count of processes to create, defaults to maximum if omitted		
		}

//var redis_ip='54.254.153.71';
//var redis_port=6379;
var arguments = process.argv.slice(2);


var port=arguments[0] || 80;
console.log("Port number on which it is starting:"+port);
var redis = require("redis"),
redis_client = redis.createClient(config.redis.port,config.redis.ip);

redis_client.on("error", function (err) {
    console.log("Error: Redis " + err);
});
//var resp = require('node-resp');


//TODO: Need to move out to config
config.baseurl=config.urls.baseurl;
config.cdnurl=config.urls.cdnurl;//"http://dev.trulymadly.com/trulymadly";
config.imageurl = config.urls.imageurl;

config.dummy_male_image=config.cdnurl+"/images/um_dummy_male.png";
config.dummy_female_image=config.cdnurl+"/images/um_dummy_female.png";

//Need better way to handle this values
var badwords = util.getBadwords;


var redis_push_notification_key="p:n";

//config values need to be taken out
//TODO: Learn about this


//TODO: in case of error
//http://stackoverflow.com/questions/20210522/nodejs-mysql-error-connection-lost-the-server-closed-the-connection
function handleDisconnect() {
	

	 pool  = service.getPool(config);	//called from service

	 pool.on('connection', function (connection) {
		  //console.log("Creating a new connection");
		});
	 
	 pool.on('enqueue', function () {
		  console.log('Waiting for available connection slot');
		});
}

handleDisconnect();


	var mainServer = function() {

	
	
	var http = require('http');
	http.globalAgent.maxSockets = 100000;

	var server = http.createServer();
	var socket_io = require('socket.io')(server,{ pingTimeout: 60000,pingInterval:25000});
	var redis_adapter = require('socket.io-redis');
	//config.redis.ip,config.redis.port
	socket_io.adapter(redis_adapter({ host: config.redis.ip, port: config.redis.port }));
	var Q = require("q");

	
	function getUserData(){
		
	}

	/*
	 * used for authentication of the user
	 */
	socket_io.use(function(socket, next){
		var handshakeData = socket.request;
		var query_params=handshakeData._query||null;

		//setting authentic to false by default
		handshakeData.authentic=false;
		
		var clientIp = handshakeData.connection.remoteAddress;
		var device_id=handshakeData.device_id;
		var app_version_code=handshakeData.app_version_code;
		var os_version_code=handshakeData.os_version_code;
		var transport=handshakeData._query.transport;
		var socket_id=socket.id;
		
		

		//console.log("redis cookie details: query param"+query_params.header);
		if(query_params&&query_params.header){
			//console.log("inside processing query params");
			
			//console.log(query_params.header);
			//TODO: if json_parse throws execption
			var basic_log={
					//user_id:"",
					ip:clientIp,
					activity:"socket",
					device_id:device_id,
					sdk_version:app_version_code,
					os_version:os_version_code,
					//tstamp:util.getMySQLTime(),
					event_info:{transport:transport,socket:socket_id},
					server_ip: config.admin.server_ip
			};
			
			var log_action1=basic_log;
			log_action1.event_type="connection";
			log_action1.event_status="connecting_recieved";
			//log_action.event_info.unique_id=unique_id;
			log_action1.tstamp=util.getMySQLTime();
			//insertIntoActionLog(log_action1);
			var header_values=JSON.parse(query_params.header);
			handshakeData.device_id=header_values.device_id;
			
			handshakeData.app_version_code=header_values.app_version_code;
			handshakeData.os_version_code=header_values.os_version_code;
			handshakeData.app_source=header_values.source;

			
			var cookies = util.parse_cookiesNew(header_values.cookie);
			var redis_key="PHPREDIS_SESSION:"+cookies.PHPSESSID;
			var miss_tm_secret = cookies.MISS_TM_SECRET;
			//console.log("redis key to fetch :"+redis_key);
			redis_client.get(redis_key,
					function(err, reply) {
				var user_id=null;
				if((err||reply==null) && (miss_tm_secret == null || miss_tm_secret == undefined)){
					//console.log("redis cookie error:"+err);
					handshakeData.authentic=false;
					return next(new Error('not authorized'));  
				}else if(miss_tm_secret != null || miss_tm_secret != undefined && miss_tm_secret == config.admin.miss_tm_secret){
					user_id = config.admin.miss_tm_id;
				}else{
					user_id=util.getUserIdFromSession(reply);
				} 
					
			//	console.log("redis fetched success reply:"+reply);
				
			//	console.log("redis reply parsed:"+user_id);
				//   	  console.log("redis user_id:"+reply);
				handshakeData.authentic=true;
			//	handshakeData.identity = reply;
				handshakeData.user_id=user_id;
				
				var log_action=basic_log;
				log_action.event_type="connection";
				log_action.event_status="connection_authenticating";
				log_action.user_id=user_id;
				//log_action.event_info.unique_id=unique_id;
				log_action.tstamp=util.getMySQLTime();
				//insertIntoActionLog(log_action);
				return  next();
			}
			);
		}
		else{
			//throw error
			return next(new Error('didnt get the query param'));  

		}

	});
	
	
//		socket_io.set('heartbeat timeout', 120); // 24h time out
	//	socket_io.set('heartbeat interval', 40);

//	util.console_log_tm("running worker");
	
//	socket_io.set('heartbeat interval', 10);
//	socket_io.set('heartbeat timeout', 10);
	
	var counter=0;
	socket_io.on('connection', function (socket) {
		counter++;
		//adding current connection 
		var handshakeData = socket.request;
		var user_id=handshakeData.user_id;
		
		var clientIp = handshakeData.connection.remoteAddress;
		var device_id=handshakeData.device_id;
		var app_version_code=handshakeData.app_version_code;
		var os_version_code=handshakeData.os_version_code;
		var transport=handshakeData._query.transport;
		var socket_id=socket.id;
		var app_source=handshakeData.app_source;

		//util.console_log_tm("app source");
		//util.console_log_tm(app_source);
		if(!app_source){
			app_source="android_app";
		}
		//util.console_log_tm("user_id"+user_id);
		var basic_log={
				user_id:user_id,
				ip:clientIp,
				activity:"socket",
				device_id:device_id,
				sdk_version:app_version_code,
				os_version:os_version_code,
				app_source:app_source,
				//tstamp:util.getMySQLTime(),
				event_info:{transport:transport,socket:socket_id},
				server_ip:config.admin.server_ip
		};
		//need to check how to validate this
		var debug=false;
		if(handshakeData.authentic==false||!user_id){
			if(!user_id)
				user_id=0;
			var log_action=basic_log;
			log_action.event_type="socket";
			log_action.event_status="connection_authentication_failed";
			log_action.tstamp=util.getMySQLTime();
			//insertIntoActionLog(log_action);
			service.insertintolog("connection","handshake_failed_after_connection","",debug,basic_log,user_id);
			//util.console_log_tm("user is non authentic.please check");
			return;
		}
		
		service.insertintolog("connection","connected","",debug,basic_log,user_id);
		socket.join(user_id);
		
		var my_id;	
				
		socket.on("ping",function(msg,callback){
			callback([]);
			}
		);
		
		socket.on("get_missed_messages",function(msg,callback){
			service.insertintolog("get_missed_messages","received",msg.unique_id,debug,basic_log,user_id);
			var time_taken_1= new Date().getTime();
			var timestamp=msg.timestamp||null;
			var query_to_add="";
			if(timestamp!=null){
				service.getMissedMessage(user_id,timestamp,config).then(function (data){
					//util.console_log_tm("device_id:"+device_id+'get_missed_messages_call: ack_sent'+" user_id:"+user_id);
					var time_taken_2= new Date().getTime();
					basic_log.time_taken = time_taken_2-time_taken_1;
					service.insertintolog("get_missed_messages","ack_sent",msg.unique_id,debug,basic_log,user_id);
					callback(data);
				})
			}else{
				service.insertintolog("get_missed_messages","ack_sent",msg.unique_id,debug,basic_log,user_id);
				callback([]);
			}
		});
		
		socket.on('disconnect', function () {
			counter--;			
			service.insertintolog("connection","disconnected","",debug,basic_log,user_id);
		});

		var chat_send_function=function (msg,callback) {
			
			//console.log("socket transport protocol::"+socket.request._query.transport);
			//console.log("chat send");
			//console.log(msg);
					//check for bad words
			//var t1 = new Date(YYYY, MM, DD, 0, 0, 0, 0)
			//:TODO need to check if user can send message to this user
			var receiver_id=msg.receiver_id;
			var sender_id=user_id;
			var unique_id=msg.unique_id;
			var tstamp=new Date().toISOString().replace(/T/, ' '). replace(/\..+/, '');
			var is_admin=msg.is_admin==true?true:false;
			var redis_key_for_uniqueness="msg_uniqueness:"+unique_id;
			msg.old_message=msg.msg;
			
			function generateReturnStatement(request){
				var deferred = Q.defer();
				var response={};
				response.msg=request.old_message;
				response.time=tstamp;
				response.sender_id=user_id;
				response.receiver_id=request.receiver_id;
				response.is_admin=is_admin;
				response.unique_id=request.unique_id;
				response.quiz_id=request.quiz_id;
				response.responseCode=200;
				response.message_type=msg.message_type
				response.metadata=msg.metadata;
				if(msg.update_spark == true || msg.update_spark == "true"){
					response.update_spark = true;
				}
				if(response.message_type == "IMAGE"){
					service.getUserAppVersion(response.receiver_id).then(
						function(data){
							if(data[0] != undefined && data[0].vcode < 230) {
								response.msg = "You've received an image. Please update your app to view the image.";
								msg.old_message= "You've received an image. Please update your app to view the image.";
							}
						},function(err){
							//console.log(err);
							deferred.resolve(response); //desktop users do not have an entry in table. resolve response as is.
							throw new Error("Some error while trying to fetch app version code for receiver.");
						}).then(function(){
							deferred.resolve(response);
						})
				}else{
					deferred.resolve(response);
				}

				return deferred.promise;
			//	return response;
			}
			

//			console.log(response);
			var checksome_matching=true;
			var tocheckbadword=msg.msg;
			if(msg.message_type=='STICKER'){
				msg.msg = msg.msg.replace('.webp', '.png');
				msg.old_message = msg.old_message.replace('.webp', '.png');
			}	
			//check for quiz type:
			if(msg.message_type=='NUDGE_SINGLE'||msg.message_type=='NUDGE_DOUBLE'||msg.message_type=='QUIZ_MESSAGE'){
				msg.msg=JSON.stringify({quiz_id:msg.quiz_id,msg:msg.msg});
				//checksome_matching=util.checkQuizCheckSum(msg.msg,user_id,msg.receiver_id,msg.message_type,config.secrets.quiz_salt,msg.checksum);							
			}
			
			//hack for message count right now
			if(msg.message_type=='NUDGE_SINGLE'||msg.message_type=='NUDGE_DOUBLE'){
				redis_client.srem("conversation_list_union:"+user_id, ""+sender_id);
				//checksome_matching=util.checkQuizCheckSum(msg.msg,user_id,msg.receiver_id,msg.message_type,config.secrets.quiz_salt,msg.checksum);							
			}
			
			if(checksome_matching==false){
				//return callback
				//TODO:ideally it should be some error message
				callback({});
			}
		
			redis_client.get(redis_key_for_uniqueness,function(err, reply) {
				if(reply==null){
			
					if(is_admin==true){
						msg.receiver_id=config.admin.admin_id;
					}
					
					service.insertintolog("chat_sent","received",unique_id,debug,basic_log,user_id,true);
					var time_taken_1= new Date().getTime();
		
					
					
					if(msg.message_type=='CD_ASK'){
						var deal_id = sender_id+"_"+receiver_id+"_"+ msg.metadata.date_spot_id+"_"+(Math.floor(Math.random()*90000) + 10000);  //last part is 5 digit random number				
						service.insertNewDeal(sender_id, receiver_id,deal_id, msg.metadata.date_spot_id);
						msg.metadata.deal_id=deal_id;
					}
					
					if(msg.message_type=='CD_VOUCHER'){
						var coupon_code = "TM"+util.randomString();
						var coupon_code = service.updateDealStatus(msg.metadata.deal_id, msg.metadata.deal_status, coupon_code, config,  msg.metadata.date_spot_id);
						msg.metadata.coupon_code=coupon_code;
					}

					if((msg.update_spark === true || msg.update_spark == "true") && msg.hash !== undefined && msg.hash !== null){
						service.acceptSpark(msg.receiver_id, user_id, msg.hash, config);
					}

					if(msg.metadata !== undefined){
						msg.msg=JSON.stringify({msg:msg.msg, metadata:msg.metadata}); 
						//msg.message_type="JSON";
					}
					var response = null;

					generateReturnStatement(msg).then(
						function(data){
							response = data;


							var message_data_old_for_queue={
								tStamp:tstamp,
								sender_id:user_id,
								receiver_id:msg.receiver_id,
								msg_content:msg.old_message,
								msg_type:msg.message_type,
								quiz_id:msg.quiz_id
							};

							if(msg.metadata !== undefined){
								msg.message_type="JSON";
							}

							var message_data={
									user_id:user_id,
									tstamp:tstamp,
									sender_id:user_id,
									receiver_id:msg.receiver_id,
									msg_content:msg.msg,
									msg_type:msg.message_type,
									unique_id:unique_id,
							};

							var message_data_reverse={
									user_id:msg.receiver_id,
									tstamp:tstamp,
									sender_id:user_id,
									receiver_id:msg.receiver_id,
									msg_content:msg.msg,
									msg_type:msg.message_type,
									unique_id:unique_id,
							};

							var message_data_old={
									tStamp:tstamp,
									sender_id:user_id,
									receiver_id:msg.receiver_id,
									msg_content:msg.msg,
									msg_type:msg.message_type,
							};


							//console.log("response data");
							//console.log(response);
							//console.log(message_data);
							//console.log(message_data_old_for_queue);

							function sendChatBackToSocket(badwordfound){
							//	console.log("sending response back");
							//	console.log(response);
								var time_taken_2= new Date().getTime();
								basic_log.time_taken = time_taken_2-time_taken_1;
								service.insertintolog("chat_sent","ack_sent",unique_id,debug,basic_log,user_id,true);
							//	console.log("calling callback");
								callback(response);
							//	console.log("called callback");
								if(badwordfound!=true){
									socket_io.to(receiver_id).emit('chat_received', response);
									redis_client.sadd("conversation_list_union:"+receiver_id, ""+sender_id);
									redis_client.sadd("mCnt:"+receiver_id, ""+sender_id);
								}
							}

							var bad_message_found=util.checkBadWords(tocheckbadword);
							if(bad_message_found==true){
							//	console.log("bad word found"+tocheckbadword);
								response.error=true;
								response.error_msg="Inappropriate content will lead to immediate removal of profile and notification to all your mutual matches of the indecent behaviour. Additionally, the same may also amount to breach of Indian laws and may be punishable thereunder.";
								service.saveBadMessage(user_id,receiver_id,tocheckbadword).then(sendChatBackToSocket(true));
							}else{
							//var promise=insertMessageinTable(message_data);
								service.checkifUserisAllowed(user_id,receiver_id, msg.update_spark).then(
										function(){
											//console.log("allowed to chat");
										return service.insertMessageinOldTable(message_data_old);
										},function(err){
											//console.log(err);
										   throw new Error("blocked");
										}).then(function(msg_id){

									redis_client.setex(redis_key_for_uniqueness,18000,msg_id);

									response.msg_id=msg_id;
									service.insertMessageInTable(message_data,msg_id);  //called from service
										//console.log(response);
										return msg_id}).
									then(function (msg_id){
										service.insertMessageInTable(message_data_reverse,msg_id);  //called from service
												return msg_id;
											}
											)
									.then(
											function(msg_id){
											service.updateMessageQueueForChatReadHack(message_data_old_for_queue);
											return msg_id;
									})
									.then(
											function(msg_id){
											service.insertMessageinQueue(message_data_old_for_queue);
											return msg_id;
							}).then(
									function(msg_id){
									service.sendDataToGCM(user_id,receiver_id,msg,msg_id,tstamp,config,redis_client);

									}).
									then(sendChatBackToSocket).then(null,function(data){
										//for error
										//came here directly
										if(data){
											callback({error:data.message,match_id:receiver_id});
										}

									});
							}
						}
					);
				}
				else{
					var response = null;

					generateReturnStatement(msg).then(
						function(data){
							response = data;
							response.msg_id=reply;
							callback(response);

						}
					);
				}
			});
		};
			
			
		socket.on('chat_sent',chat_send_function );
		
		socket.on('chat_sent_array',function(msg_array,callback) {
			//console.log("came at chat_sent_array");
			
			var chat_sent_array_length=msg_array.length;
			var call_back_recieved=0;
			//console.log(msg_array);
			
		    var callback_data = [];
		    
		    var callback_hack=function(json_data){
		    	call_back_recieved++;
		    	//console.log("getting callback in array");
		    	//console.log(json_data);
		    	if(!json_data)return;
		    	callback_data.push(json_data);
		    	if(call_back_recieved==chat_sent_array_length){
					//console.log(callback_data);
					callback(callback_data);
		    	}
		    }
	
		    
			for (var i = 0; i < msg_array.length; i++) {
				chat_send_function(msg_array[i],callback_hack);
			}
	
		}
		);
		
		socket.on('get_chat_metadata', function (msg,callback) {
			//:TODO need to check if user can send message to this user
			//service.insertintolog("get_chat_metadata","received",msg.unique_id);

			var sender_id_array=msg.match_ids;
			var receiver_id=user_id;
			var is_admin=msg.is_admin==true?true:false;
			service.getMessageQueueData(sender_id_array,receiver_id,config).then(
					function(result){
					//	service.insertintolog("get_chat_metadata","ack_sent",msg.unique_id);
						callback(result);
					});
					

			//updateMessageinQueue(tstamp,last_seen_msg_id,user_id,receiver_id).then(insertMessageinTable(message_data,null)).then(sendChatReadBacktoUser());
		});
		
		
		

		socket.on('get_user_metadata', function (msg,callback) {
		//	console.log('get_user_metadata call');
		//	console.log(msg);

			//service.insertintolog("get_user_metadata","received",msg.unique_id);


			//log_action.event_info.msg=msg.msg;
			//insertIntoActionLog(log_action);
		//	console.log("for user:"+user_id);
					//:TODO need to check if user can send message to this user
			var sender_id_array=msg.match_ids;
			service.getUserMetaData(sender_id_array,user_id,config,app_version_code).then(
					function(result){		
				//		service.insertintolog("get_user_metadata","ack_sent",msg.unique_id);
						callback(result);
					});
					
		});
		
		socket.on('error', function (err) { console.error(err.stack); // TODO, cleanup
		});
		
		
		socket.on('chat_typing', function (msg,callback) {	
			
			if(msg){
			var sender_id=msg.receiver_id;		
			var response={
					sender_id:user_id,
					receiver_id:sender_id
			}
			socket_io.to(sender_id).emit('chat_typing_listener', response);	
			}
			callback({responseCode:200});
		});
		
		socket.on('chat_read', function (msg,callback) {
		 //service.insertintolog("chat_read","received","");
			
		//	console.log("chat_read");
			//:TODO need to check if user can send message to this user
			var sender_id=msg.sender_id;
			var last_seen_msg_id=msg.last_seen_msg_id;
			var tstamp=new Date().toISOString().replace(/T/, ' '). replace(/\..+/, '')  ;
			var response={
					tstamp:tstamp,
					sender_id:sender_id,
					receiver_id:user_id,					
					last_seen_msg_id:msg.last_seen_msg_id
			}
			redis_client.srem("mCnt:"+user_id, ""+sender_id);
			redis_client.srem("conversation_list_union:"+user_id, ""+sender_id);

			function sendChatReadBacktoUser(){	
			//	console.log("sending read callback back to user");
				socket_io.to(sender_id).emit('chat_read_listener', response);		
				//service.insertintolog("chat_read","ack_sent","");
				callback(response);
			}
			service.updateMessageinQueue(tstamp,last_seen_msg_id,sender_id,user_id).then(sendChatReadBacktoUser());
		});
	});
	//socket_io.listen(server);

	 return server;

	}

	if (!sticky.listen(mainServer(), port, sticky_options)) {
		  // Master code
		mainServer().once('listening', function() {
			console.log('server started on 3000 port');
		});
	} else {
	  // Master code
	}



