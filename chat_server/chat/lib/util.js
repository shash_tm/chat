var fs=require('fs');
var  ini = require('ini')
var crypto = require('crypto');
var cluster = require('cluster');

exports.parsedConfig = (function() {
	return ini.parse(fs.readFileSync('/var/www/html/config/config.ini', 'utf-8'))
})()

var badwords = exports.getBadwords = (function(){
	var badwords_new = fs.readFileSync('badwords.txt').toString().split("\n");
	var badwordsVar=[];
	for(i in badwords_new) {
		var word=badwords_new[i].toLowerCase().trim();
		badwordsVar[word]=word;
	}
	return badwordsVar;
})();

exports.getMySQLTime = function(){
	return new Date().toISOString().replace(/T/, ' '). replace(/\..+/, '');
}

exports.checkBadWords = function(msg){
	//console.log("msg:"+msg);
	try{
		if(msg == undefined || msg == null)
			return false;
		var msg_split=msg.split(" ");
		var bad_message_found=false;
		for(var j=0;j<msg_split.length;j++){
			//var word_to_check=[msg_split[j].toLowerCase()];
			//	console.log("word to check:"+word_to_check);
			//console.log("bad word check"+badwords[word_to_check]);
			
			//iif(typeof badwords[msg_split[j].toLowerCase()]){
			var word=msg_split[j].toLowerCase();
			var badwordsArray = badwords;
			//console.log("word to check:"+word);
		//	console.log("index of"+badwordsArray[word]);
			if(word.length>0 && badwordsArray[word]==word){
			//	console.log("bad word found"+word);
				bad_message_found=true;
				break;
			}
		}
		return bad_message_found;
	}catch(ex){
		console.log("unable Check for badwords in  "+msg);
		return false;
	}
}

var calculateCheckSum = exports.calculateCheckSum = function(str, config){
	//console.log("m5 string"+str);
	//console.log("secret key"+config.secrets.salt);
	return crypto.createHash('md5').update(str + '|'+ config.secrets.salt).digest('hex');
}

//Following is not used
exports.checkQuizCheckSum = function(quiz_id,my_id,match_id,message_type,salt,checksum){
	return crypto.createHash('md5').update(quiz_id+my_id+match_id+message_type+salt).digest('hex')==checksum;
}

exports.generateMessageLink = function(user_id,match_id,config){
//	console.log("generateProfileLink user_id:"+user_id+":match_id:"+match_id);

	var str = user_id +'|' +match_id;
	var mid = calculateCheckSum(str,config);
	return config.baseurl+"/msg/message_full_conv.php?match_id="+match_id+"&mesh="+mid;
}
		
	
	
exports.generateProfileLink = function(user_id, match_id,config,app_version_code){
//	console.log("generateProfileLink user_id:"+user_id+":match_id:"+match_id);

	var str = user_id +'|' +match_id;
	var mid = calculateCheckSum(str,config);
	var versionArray = ["2.0.6","2.0.5","2.0.4","2.0.3","2.0.2","2.0.1","2.0.0","1.9.1","1.9.0","1.8.9","1.8.8","1.8.7","1.8.6","1.8.5","1.7.8","1.7.6"];
	if(app_version_code != undefined && app_version_code != null && versionArray.indexOf(app_version_code) > -1 )
		return config.urls.old_baseurl+"/profile.php?pid="+mid+'_'+match_id;
	else
		return config.baseurl+"/profile.php?pid="+mid+'_'+match_id;
}

//Following is not used
var getRedisCounterKey = exports.getRedisCounterKey = function(worker_id){
	var key=null;
	if(worker_id){
		key="chat_socket_counter_"+worker_id;
	}
	return key;
}

//Following is not used
var getRedisSocketCountKey = exports.getRedisSocketCountKey = function(worker_id){
	var key=null;
	if(worker_id){
		key="chat_socket_counter_from_nodejs_"+worker_id;
	}
	return key;
}

//Following is not used
var debugUser = exports.debugUser = function(user_id){
	redis_client.decr(redis_counter_key);
	var key=null;
	if(worker_id){
		key="chat_socket_counter_from_nodejs_"+worker_id;
	}
	return key;
}

exports.capitalizeFirstLetter = function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}



/**
 * code to parse the cookie.
 * Need to move out to a separate file
 * @param _cookies
 * @returns {___anonymous2852_2853}
 */
exports.parse_cookiesNew = function(_cookies) {
	var cookies = {};
	//console.log("cookie length"+(_cookies!=null));

	if(_cookies){
		//console`.log("cookie length"+_cookies!=null);
//TODO: Check the cookie datatype here
		for (var i = 0; i < _cookies.length; i++) {
			var key = _cookies[i].key;
			var value=_cookies[i].value;
			if(key){
				cookies[key]=value;
				//console.log("cookies key:"+key+": value:"+value);
			}
		}
	}
	return cookies;
}

var trim = exports.trim = function(str) {
	if(str)
	return str.replace(/"/g,"");
}
/**
 * TODO: Check all error and edge conditions
 * @param cookie
 * @returns
 */
exports.getUserIdFromSession = function(redis_session_details){
	var user_id=null;
	if(redis_session_details==null)
		return null;
	var split = redis_session_details.split(';');
	for (i = 0; i < split.length; i++) { 
		var key=split[i].split('|');
		if(key[0]=="user_id"){
			user_id=trim(key[1].split(':')[2]);
			break;
		}
	}
	return user_id;
}

var console_log_tm = exports.console_log_tm = function(initial,data){
	//get the worker_id here
	if(data)
		{
	console.log("worker_id:"+cluster.worker.id+" "+initial+":Data:"+data);
		}
	else{
	console.log("worker_id:"+cluster.worker.id+" "+initial);
	}

}

exports.randomString = function()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}



/*
socket_io.use(function(socket, next){
	console.log("came here to second part");
	next();
});
*/
