var pool;
var mysql = require('mysql');
var Q = require("q");
var util = require("./util");
var fs = require("fs")
var redis_push_notification_key="p:n";
var request = require('request');

exports.getPool = function(config){
	pool = mysql.createPool({
		  connectionLimit : 500,
		  
		  connectTimeout:20000,
		  //
		  acquireTimeout:20000,
		  host            : config.database.hostname,
		  user            : config.database.username,
		  password        :config.database.password,
		  database		:config.database.database,
		  charset  : 'latin1_swedish_ci'
		});
	return pool;
}

exports.insertMessageInTable = function(message_data,msg_id){
	var deferred = Q.defer();
	message_data.msg_id=msg_id;
	pool.getConnection(function(err, connection){
		//console_log_tm(connection);
		//console.log(connection);
		if(err){
			if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
			connection.query('INSERT INTO user_messages SET ?', message_data, function(err, result) {
				connection.release();
				if(!err){
					deferred.resolve(msg_id);
				}
				else{
					console.log("mysql failed"+err);
					//TODO: Need to check this
					deferred.reject({error:"Some error"});
		//			callback("error");
				}		
			});
		}
	});
	return deferred.promise; // the promise is returned
}

//This should be deprecated.We should use redis for this
exports.insertMessageinOldTable = function(message_data){
//	console.log("counter 1");
	var deferred = Q.defer();
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}
		else{
	connection.query('INSERT INTO current_messages_new SET ?', message_data, function(err, result) {
	    connection.release();
		if(!err){
			//console.log(result);
			deferred.resolve(result.insertId);
		}
		else{

			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
	}
});
	return deferred.promise // the promise is returned
}

/*
 * check if user is allowed to chat
 */
exports.checkifUserisAllowed = function(user_val_1,user_val_2, update_spark){

	//console.log("checkifUserisAllowed");
	var deferred = Q.defer();
	if(user_val_1=='admin'||user_val_2=="admin"){
		 deferred.resolve();
		 return deferred.promise;
	}
	var user1=parseInt(user_val_1);
	var user2=parseInt(user_val_2);
	
	var user1_mm=(user1<user2)?user1:user2;
	var user2_mm=(user1<user2)?user2:user1;
	
	//console.log(user_val_1<user_val_2);
	//console.log(user1_mm);
	//console.log(user2_mm);
	
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query('select * from messages_queue where (sender_id=? and receiver_id=?) or (receiver_id=?  and sender_id=?)',
			[user1_mm,user2_mm,user1_mm,user2_mm],
			function(err, result) {
	    connection.release();

		if(!err){				
			if(result.length>=1){
				if(result[0].blocked_by!=null){
					deferred.reject({error:"blocked"});
				}
				//result=null;
				//console.log("we can message");
				deferred.resolve(result);
			}else if(update_spark == true || update_spark == "true"){
				//check if user actually sparked
				pool.getConnection(function(err, connection) {
					if (err) {
						if (connection != undefined && connection != null)
							connection.release();
						console.log('db error', err);
						deferred.reject({error: "Some error"});
					} else {
						connection.query('select * from user_spark where user1=? and user2=?',
							[user_val_2, user_val_1],
							function (err, result) {
								connection.release();

								if (!err) {
									if (result.length == 1) {
										deferred.resolve(result);
									} else {
										deferred.reject();
									}
								} else {
									deferred.reject();
								}
							});
					}
				});
			}
			else{
				//console.log("we can not message");
				deferred.reject();
			}
		}
		else{

			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
		}
	});
	return deferred.promise // the promise is returned
}


exports.insertMessageinQueue = function(message_data){
	//console.log("counter 4");
	var deferred = Q.defer();
	
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query("INSERT INTO `messages_queue` SET ? ON DUPLICATE KEY UPDATE msg_content=VALUES(msg_content), tStamp=VALUES(tStamp), msg_type=VALUES(msg_type)", message_data,
			function(err, result) {
	    connection.release();

		if(!err){
			deferred.resolve(result);
			//console.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
		}
	});
	return deferred.promise // the promise is returned
}

/**
 * main call to get the mesage list
 */
exports.getMissedMessage=function (user_id,timestamp,config){
	var deferred = Q.defer();
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query("SELECT um.tstamp as time,um.sender_id,um.receiver_id,um.msg_content as msg,um.msg_type as message_type,um.msg_id,um.unique_id  FROM user_messages um " +
		" WHERE um.user_id = ? and um.tstamp>=?", [user_id,timestamp], function(err, results) {
		//TODO: for err need to see what to do
	    connection.release();

		if(!err){
			//convert timestamp in the result
			sparkFlags = new Object();
			sparkAccepted = new Object();
			for(var i = results.length-1; i >= 0; i--) {

				//Creating an array of sparks received by user.
				if(results[i].message_type == 'SPARK'){
					var sparkFlag = {flag:0, index:0, sender:0, receiver:0, accepted: false};
					sparkFlag.flag = 1;
					sparkFlag.index = i;
					sparkFlag.receiver= results[i].receiver_id;
					sparkFlag.sender = results[i].sender_id;
					sparkFlag.msg_id = results[i].msg_id;
					sparkFlags[results[i].sender_id] = sparkFlag;
				}
				if(results[i].message_type=='NUDGE_SINGLE'||results[i].message_type=='NUDGE_DOUBLE'||results[i].message_type=='QUIZ_MESSAGE'){
					try{
						var json_for_quiz=JSON.parse(results[i].msg);						
						results[i].quiz_id=json_for_quiz.quiz_id;//{quiz_id:msg.quiz_id,msg:msg.msg};
						results[i].msg=json_for_quiz.msg;
					}catch(ex){
						console.log("unable to parse json for message id: "+results[i].msg_id);
					}
					//checksome_matching=checkQuizCheckSum(msg.msg,user_id,msg.receiver_id,msg.message_type,config.secrets.quiz_salt,msg.checksum);			
				}

				if(results[i].message_type=='JSON'){
					var json_for_ouput;
					try{
						json_for_ouput=JSON.parse(results[i].msg);			
					}catch(ex){
						console.log("unable to parse json for message id: "+results[i].msg_id);
					}
					try {
						if(json_for_ouput.metadata.spark_accepted != undefined &&
							(json_for_ouput.metadata.spark_accepted == true || json_for_ouput.metadata.spark_accepted == 'true')){
							var receiver  = results[i].receiver_id;
						/*	var sparkFlagT = sparkFlags[receiver];
							sparkFlagT.flag = 2;
							sparkFlags[receiver] = sparkFlagT;*/
							sparkAccepted[receiver] = true;
						}
						results[i].msg = json_for_ouput.msg;
						results[i].message_type = json_for_ouput.metadata.message_type != undefined ? json_for_ouput.metadata.message_type : 'TEXT';
						results[i].metadata = json_for_ouput.metadata;
					}catch(ex){
						console.log("No metadata/another property found for this message"+results[i].msg_id);
					}
				}
				if(results[i].sender_id==config.admin.admin_id){
					results[i].sender_id="admin";
				}
				if(results[i].receiver_id==config.admin.admin_id){
					results[i].receiver_id="admin";
				}
				results[i].time=new Date(results[i].time).toISOString().replace(/T/, ' '). replace(/\..+/, '')  ;

			}

			//copying the whole array to new array, ignoring spark messages which have not been replied
			// crappiest code flow, redundant loop. But Oh, it's too tough for android guys to handle it on client.
			var newResults = [];
			var j = 0;
			for(var i = results.length-1; i >= 0; i--) {
				if(results[i].message_type == 'SPARK'){
					if(sparkAccepted[results[i].sender_id] !=undefined && sparkAccepted[results[i].sender_id] == true){
						//spark has been accepted
					}
					else{
						continue;
					}
				}
				newResults[j] = results[i];
				j++;
			}

			deferred.resolve(newResults);
		}
		else{
			//TODO: need to think more on this
			console.log(err);
			deferred.reject({error:"not able to save the data"});
			//callback({error:"DB Error"});
		}
	});	
		}
	});
	return deferred.promise;
}

exports.insertintolog = function(event_type,event_status,unique_id,debug,basic_log,user_id,debug_override){
	debug_override = typeof debug_override !== 'undefined'?debug_override:false;
	debug_override=false;
	//	console.log(debug);
	if(basic_log.time_taken == null || basic_log.time_taken == undefined)
		basic_log.time_taken = 0;
	if(debug || debug_override){
		var log_action=basic_log;		
		log_action.event_type=event_type;
		log_action.event_status=event_status;
		log_action.event_info.unique_id=unique_id;
		log_action.tstamp=util.getMySQLTime();
		insertIntoActionLog(log_action, true);   //pass param as true if we need to do logging via file write, fasle for insert into mysql
		if(debug){
	//		 util.console_log_tm("event_type:"+event_type+'event_status:'+ event_status+" user_id:"+user_id);
	//		 console.log(log_action.event_info);
		 
		}
	}
}

/**
 * adding in action_log_new
 * @param user1
 * @param user2
 * @param msg
 * @returns
 */
var insertIntoActionLog = exports.insertIntoActionLog = function(log_data, writeToFile){
    if(!writeToFile)
    	insertLogDataToMysql(log_data);
    else
    	writeLogInsertsToFile(log_data);
}

exports.updateMessageinQueue = function(last_seen,last_seen_msg_id,sender_id,receiver_id){
	var deferred = Q.defer();
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query("UPDATE messages_queue SET last_seen =?, last_seen_msg_id = ? WHERE sender_id =? AND receiver_id = ? and last_seen<=tStamp"
			, [last_seen,last_seen_msg_id,sender_id,receiver_id],
			function(err, result) {
			    connection.release();

		if(!err){
			deferred.resolve(result);
			//console.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
		}
	});
	return deferred.promise // the promise is returned
}

exports.updateMessageQueueForChatReadHack = function(message_data_old_for_queue){
	var deferred = Q.defer();
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query("UPDATE messages_queue SET last_seen =? WHERE sender_id =? AND receiver_id = ? and last_seen<tStamp"
			, [message_data_old_for_queue.tStamp,message_data_old_for_queue.receiver_id,message_data_old_for_queue.sender_id],
			function(err, result) {
			    connection.release();

		if(!err){
			deferred.resolve(result);
			//console.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
		}
	});
	return deferred.promise // the promise is returned
}
		


exports.saveBadMessage = function(user1,user2,msg){
	var deferred = Q.defer();
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
		    deferred.reject({error:"Some error"});
		}else{
	connection.query("INSERT into badMessages_exchanged values(?,?,?,now(),?)"
			, [user1,user2,msg,null],
			function(err, result) {
			    connection.release();

		if(!err){
			deferred.resolve(result);
			//console.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
		}
	});
	return deferred.promise // the promise is returned
}

exports.getMessageQueueData = function(sender_id_array,receiver_id,config){
	var deferred = Q.defer();
	
	var values_array=[];
	
	for(var i = 0; i < sender_id_array.length; i++) {
		
		var val=parseInt(sender_id_array[i],10);
		if(sender_id_array[i]=="admin"){
			val=config.admin.admin_id;
		}
		if(val){
			values_array.push(val);
		}
	}
	
	//console.log("get message queue data");
	//console.log(values_array);
	//console.log(receiver_id);
	
	if(values_array.length>0){
		var sender_ids=values_array.join(",");

		pool.getConnection(function(err, connection){
			if(err){
			    if(connection != undefined && connection != null)
			    	connection.release();
			    console.log('db error', err);
			    deferred.reject({error:"Some error"});
			}else{
	connection.query("select receiver_id as match_id,last_seen_msg_id,last_seen as last_seen_msg_time from messages_queue where receiver_id in (?) and sender_id = ? "
			, [sender_ids,receiver_id],
			function(err, result) {
			    connection.release();

		if(!err){
			//console.log(result);;
			for(var i = 0; i < result.length; i++) {
				//console.log("result time format");
				//console.log(results[i].time);
				//we don't show the timestamp for admins
				
				if(result[i].match_id==config.admin.admin_id){
					result[i].last_seen_msg_time='0000-00-00 00:00:00';						
					result[i].match_id="admin";
			}
				
				else{
					var timestamp=Date.parse(result[i].last_seen_msg_time);
					if(timestamp){
						result[i].last_seen_msg_time=new Date(timestamp).toISOString().replace(/T/, ' '). replace(/\..+/, '')  ;
					}
					else{
						result[i].last_seen_msg_time="";
					}
			
				}
			}
			deferred.resolve(result);
			///.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
			}
		});
	}else{
		 deferred.resolve({});
	}
	return deferred.promise // the promise is returned
}


/*
 * to get the metadata of the users
 */
var getUserMetaData = exports.getUserMetaData = function(sender_id_array,user_id, config,app_version_code){
	var deferred = Q.defer();
	var values_array=[];
	
	for(var i = 0; i < sender_id_array.length; i++) {
		var val=parseInt(sender_id_array[i],10);
		if(sender_id_array[i]=="admin"){
			val=config.admin.admin_id;
		}
		if(val){
			values_array.push(val);
		}
	}

	
	//console.log("sender_id_array");
	//console.log(sender_id_array);
	
	if(values_array.length>0){
		var sender_ids=values_array.join(",");
		pool.getConnection(function(err, connection){
			if(err){
			    if(connection != undefined && connection != null)
			    	connection.release();
			    console.log('db error', err);
			    deferred.reject({error:"Some error"});
			}else{
	connection.query("select up.thumbnail as  profile_pic,u.user_id as profile_id,u.fname as fname,u.gender as gender from " +
			"user u left join user_photo up on up.user_id=u.user_id and up.status ='active' and up.is_profile = 'yes' and up.admin_approved='yes' " +
			"where u.user_id in (?) "
			, [sender_ids],
			function(err, result) {
	    connection.release();

		if(!err){
			for(var i = 0; i < result.length; i++) {
				if(result[i].profile_id==config.admin.admin_id){
					result[i].profile_id="admin";
				}
				if(result[i].profile_pic){
					result[i].profile_pic=config.imageurl+result[i].profile_pic;
				}
				else{
					result[i].profile_pic=result[i].gender=="M"?config.dummy_male_image:config.dummy_female_image;
				}
				result[i].profile_link=util.generateProfileLink(user_id, result[i].profile_id,config,app_version_code);
				if(config.admin.miss_tm_id==result[i].profile_id)
					result[i].is_miss_tm="true";
				else
					result[i].is_miss_tm="false";
			}
			deferred.resolve(result);
			//console.log("sending message to :"+receiver_id);
		}
		else{
			console.log("err recieved"+err);
			deferred.reject(err);
		}		
	});
			}
		});
	}else{
		 deferred.resolve({});
	}
	return deferred.promise // the promise is returned
}

/*
 * my_id -> we have to fetch my data to send to the match_id
 * mgs_id should be deprecated
 */
exports.sendDataToGCM = function(my_id,match_id,msg,msg_id,tstamp,config,redis_client){
	//console.log("counter 5");

	var my_arr=[my_id];
	//we will be using the same function.later on this needs to be move to redis
	//TODO:
	var promise=getUserMetaData(my_arr,match_id,config).then(
			function(results){
				var my_data=null;
				for(var i = 0; i < results.length; i++) {
					if(results[i].profile_id==my_id){
						my_data=results[i];
						break;
					}
				}
				//if we got the data
				if(my_data){
					//profile_pic,profile_id, fname,u.gender as gender
					var push_notification_object={};
				//	var message_url=generateMessageLink(match_id,my_data['profile_id']);

					push_notification_object.push_type = "MESSAGE";
					push_notification_object.title_text=my_data['fname'];

					if(msg.update_spark == true || msg.update_spark == "true"){
						push_notification_object.content_text= util.capitalizeFirstLetter(my_data['fname'])+"  just accepted your Spark.";
						push_notification_object.title_text="Things just got interesting";
					}
					else if(msg.message_type == "TEXT"){
						push_notification_object.content_text = ( msg.msg.length ) > 30 ?  msg.msg.substr( 0, 29 ) +'...' : msg.msg ;
					}
					else if(msg.message_type=="STICKER"){
						push_notification_object.content_text= util.capitalizeFirstLetter(my_data['fname'])+" has sent you a sticker."; //""content_text"=>ucfirst($user_data['name'])."  has sent you a sticker.",
					}
					else{
						push_notification_object.content_text= msg.old_message;//""content_text"=>ucfirst($user_data['name'])."  has sent you a sticker.",
					}

					push_notification_object.is_admin_set=my_data['profile_id']=="admin"?1:0;
					push_notification_object.pic_url=my_data['profile_pic'];
					push_notification_object.ticker_text="Message from "+my_data['fname'];
					push_notification_object.message_url=util.generateMessageLink(match_id,my_data['profile_id'],config);
					push_notification_object.match_id=my_id;
					push_notification_object.msg_id=msg_id;
					push_notification_object.tstamp=tstamp;
					push_notification_object.priority="high";
					//push_notification_object.message_type=msg.message_type;

					var push_to_redis={};
					push_to_redis.push_data=push_notification_object;
					push_to_redis.user_id=match_id;
					push_to_redis.sender_id=my_id;
					redis_client.lpush(redis_push_notification_key,JSON.stringify(push_to_redis));	
				}
			}
	);
}

//insert logging data into mysql
function insertLogDataToMysql(log_data){
	 var sql = "INSERT delayed into action_log_new " +
		"(user_id, ip, activity, event_type, event_status, " +
		"tstamp, device_id, sdk_version, os_version,time_taken,source,event_info, user_agent) " +
		"values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
	
		}else{
		    //console.log('running sql query', sql);

	connection.query(sql,[log_data.user_id,log_data.ip,log_data.activity,
	                      log_data.event_type,log_data.event_status,log_data.tstamp,
	                      log_data.device_id,log_data.sdk_version,log_data.os_version,log_data.time_taken,log_data.app_source,JSON.stringify(log_data.event_info), "socket_server_"+log_data.server_ip] ,
				function(err, result) {
				    connection.release();
			
					if(!err){
						//console.log("sending message to :"+receiver_id);
					}
					else{
						console.log("err recieved"+err);
					}		
			});
		}
    });
}

//write logging data to a file. 
function writeLogInsertsToFile(log_data){
	var directory = 'sql-dumps';
	var difference = 0;
	var currentTStamp  = (new Date).getTime();
	var queryValues = log_data.user_id+"<->"+log_data.ip+"<->"+log_data.activity+"<->"+ 
						log_data.event_type+"<->"+log_data.event_status+"<->"+log_data.tstamp+
						"<->"+log_data.device_id+"<->"+log_data.sdk_version+"<->"+log_data.os_version+"<->"+log_data.time_taken+"<->"+
						log_data.app_source+"<->"+JSON.stringify(log_data.event_info)+"<->"+ "socket_server_"+log_data.server_ip +"|-|";

	var lastFileName="";
	var currentDate=new Date();
	var currentMonth = currentDate.getMonth()+1;
	var currentFileName = ""+currentDate.getFullYear()+currentMonth+currentDate.getDate()+currentDate.getHours()+currentDate.getMinutes();
	fs.appendFile(directory+"/"+currentFileName, queryValues, function (err) {
		if (err) {
	       return console.error(err);
	    }
	});
}

exports.insertNewDeal = function(user1, user2,deal_id, datespot_id){
	var sql = "INSERT INTO user_deals (datespot_hash_id, user1, user2, created_tstamp, last_updated_tstamp, status, app_deal_id) values(?,?,?, now(), now(), 'waiting',?)";
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
	
		}else{
		    //console.log('running sql query', sql);
	
	connection.query(sql,[datespot_id,user1,user2,deal_id] ,
				function(err, result) {
				    connection.release();
			
					if(!err){
						//console.log("sending message to :"+receiver_id);
					}
					else{
						console.log("err recieved"+err);
					}		
			});
		}
	});
}

exports.updateDealStatus = function(deal_id, status, coupon_code, config, datespot_id){
	if(status == 'accepted'){
		request(config.urls.baseurl+'/curatedDeals/dealSmsService.php?action=send_deal_sms&deal_id='+deal_id, function (error, response, body) {
		    //do nothing as of now
		});
	}
	var sql = "UPDATE user_deals set status=?, coupon_code=?, last_updated_tstamp=now() where app_deal_id=?";
	var couponSql = "select coupon_code,multiple_coupon from datespots where hash_id = ?" ;
	pool.getConnection(function(err, connection){
		if(err){
		    if(connection != undefined && connection != null)
				connection.release();
		    console.log('db error', err);
	
		}else{
		    //console.log('running sql query', sql);
			connection.query(couponSql,[datespot_id] ,
					function(err, result) {
						if(!err){
							
							if(result[0].coupon_code != "" && result[0].coupon_code != null){
								if(result[0].multiple_coupon == 1){
									var couponStr = result[0].coupon_code;
									var couponsArr = couponStr.split(",");
									coupon_code = couponsArr[0];
									if(couponsArr.length > 1)
										couponStr = couponStr.replace(coupon_code+',','');
									else {		//LAST COUPON CODE BEING USED. DEACTIVATE DEAL AS WELL
										couponStr = couponStr.replace(coupon_code, '');
										request(config.urls.baseurl+'/curatedDeals/dealSmsService.php?action=deactivate&deal_id='+datespot_id, function (error, response, body) {
											//do nothing as of now
										});
									}
									connection.query('update datespots set coupon_code=? where hash_id=?', [couponStr, datespot_id],
										function (err, result) {
											//connection.release();
											if (!err) {

											}
											else {
												console.log("err recieved" + err);
											}
										});

								}else
									coupon_code = result[0].coupon_code;
							}
							connection.query(sql,[status,coupon_code,deal_id] ,
									function(err, result) {
									    connection.release();
										if(!err){
											//console.log("sending message to :"+receiver_id);
										}
										else{
											console.log("err recieved"+err);
										}
							});
						}
						else{
							connection.release();
							console.log("err recieved"+err);
						}		
			});
		}
	});
}

exports.getUserAppVersion = function(user_id){

	//console.log("checkifUserisAllowed");
	var deferred = Q.defer();
	var user1=parseInt(user_id);

	pool.getConnection(function(err, connection){
		if(err){
			if(connection != undefined && connection != null)
				connection.release();
			console.log('db error', err);
			deferred.reject({error:"Some error"});
		}else{
			connection.query('select  user_id, CAST(app_version_code AS UNSIGNED) as vcode, install_tstamp from user_gcm_current where user_id=? order by vcode desc limit 1',
				[user1],
				function(err, result) {
					connection.release();

					if(!err){
						if(result.length==1){
							if(result[0].app_version_code!=null){
								deferred.resolve(result[0].app_version_code);
							}
							//result=null;
							//console.log("we can message");
							deferred.resolve(result);
						}
						else{
							//console.log("we can not message");
							deferred.reject();
						}
					}
					else{
						console.log("err recieved"+err);
						deferred.reject(err);
					}
				});
		}
	});
	return deferred.promise // the promise is returned
}

exports.acceptSpark = function (sender_id, receiver_id, hash, config){
	var deferred = Q.defer();
//	console.log(config.urls.baseurl+'/spark/acceptspark.php?' + 'action=update_spark&spark_action=accepted&hash='+hash+'&sender_id='+sender_id+'&receiver_id='+receiver_id);
	request(config.urls.baseurl+'/spark/acceptspark.php?' +
		'action=update_spark&spark_action=accepted&hash='+hash+'&sender_id='+sender_id+'&receiver_id='+receiver_id, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			deferred.resolve(body);
		}else{
			console.log("ERROR while accepting spark --->> "+config.urls.baseurl+'/spark/acceptspark.php?' +
				'action=update_spark&spark_action=accepted&hash='+hash+'&sender_id='+sender_id+'&receiver_id='+receiver_id);
			deferred.reject(error);
		}
	});

	return deferred.promise;
}

var checkUserSparkStatus = exports.checkUserSparkStatus  = function(user1, user2){

	//console.log("checkifUserisAllowed");
	var deferred = Q.defer();

	pool.getConnection(function(err, connection){
		if(err){
			if(connection != undefined && connection != null)
				connection.release();
			console.log('db error', err);
			deferred.reject({error:"Some error"});
		}else{
			connection.query('select * from user_spark where (user1 = ? and user2 = ?) or (user1 = ? and user2 = ?)',
				[user1, user2, user2, user1],
				function(err, result) {
					connection.release();

					if(!err){
						if(result.length==1){
							if(result[0].user_spark!=null){
								deferred.resolve(result[0].user_spark);
							}
							//result=null;
							//console.log("we can message");
						}
						else{
							//console.log("we can not message");
							deferred.reject();
						}
					}
					else{
						console.log("err recieved"+err);
						deferred.reject(err);
					}
				});
		}
	});
	return deferred.promise; // the promise is returned
};


